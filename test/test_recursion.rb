require 'helper'

class TestRecursion < Test::Unit::TestCase
  def setup
    class << self
      deftail(:tail_factorial) { |acc, n=1| n < 2 ?  acc : [n * acc, n-1] }
      def factorial(n)
        tail_factorial(1, n)
      end
    end
  end

  def test_recursive_functions_produce_appropriate_output
    assert_equal(120, factorial(5))
  end
end
