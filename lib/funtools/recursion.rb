#!/usr/bin/env ruby

class Object
  # Public: Define a method in the current scope which will execute a given
  # block recursively until a fixpoint is reached.
  #
  # sym   - Symbol defining the name of the method to be created.
  # block - Block containing the logic for the function to be created.
  #
  # Returns nothing.
  def deftail(sym, &block)
    self.send(:define_method, sym) do |*n|
      last = nil
      [1].cycle.reduce(n) { |c, e| break c if last == c; last = c; block[c] }
    end
  end
end
